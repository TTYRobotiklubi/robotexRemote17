/*
 *  4 1
 *  3 2  LEDide numbrid kui pult on püsti
 */

const int ButtonS2 = 15;
const int ButtonS3 = 16;
const int ButtonS1 = 14;
const int ButtonS4 = 17;

const int LED_D3   = 1;
const int LED_D2   = 2;
const int LED_D4   = 0;
const int LED_D1   = 3;

#define MATCH_OFF 0
#define MATCH_ON 1
#define MATCH_INIT 2

#define D4_R  (1<<0)
#define D4_G  (1<<1)
#define D4_B  (1<<2)
#define D3_R  (1<<3)
#define D3_G  (1<<4)
#define D3_B  (1<<5)
#define D2_R  (1<<6)
#define D2_G  (1<<7)
#define D2_B  (1<<8)
#define D1_R  (1<<9)
#define D1_G  (1<<10)
#define D1_B  (1<<11)

#define VOTE_LEDS  (D1_R | D2_B | D3_G | D4_B | D4_R)

//Pin connected to latch pin (ST_CP) of 74HC595
//Pin connected to clock pin (SH_CP) of 74HC595
//Pin connected to Data in (DS) of 74HC595
const int latchPin = 6;
const int clockPin = 5;
const int dataPin = 4;


int field = 'A';
int goalBuffer[] = {'a', 0, 0, 'A', 'C', 'K', '-','-','-','-','-','-'};
static int stateMenu = 0;
bool pingACK_A = 0;
bool pingACK_B = 0;
bool stopACK_A = 0;
bool stopACK_B = 0;

void setup() 
{  
  //set 595 pins to output 
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  
  // Initialize buttons
  pinMode(ButtonS2, INPUT_PULLUP); 
  pinMode(ButtonS1, INPUT_PULLUP);
  pinMode(ButtonS3, INPUT_PULLUP);
  pinMode(ButtonS4, INPUT_PULLUP);
  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  
  ledUpdate(D4_G | D2_G | D2_B | D3_G | D3_B);
  if(digitalRead(2))    //Valjaku valimine A/B
  {
    field = 'B';
  }
  else field = 'A';
  delay(300);
  goalBuffer[1] = field;
  //PINGING CYCLE BEFORE ACTIVATING START SEQUENCE
  while(digitalRead(ButtonS4));
  ledUpdate(D2_B | D3_B);
  delay(150);
  while(!digitalRead(ButtonS4));
  delay(150);
  ledUpdate(D2_B | D3_B | D4_G);
  //pinging cycle
  while(digitalRead(ButtonS4))
  {
    //COLOR FEEDBACK BASED ON PINGING ACTIVITY
    if(pingACK_A && !pingACK_B)
    {
      ledUpdate(D2_B | D3_G| D4_G);
    }
    else if(pingACK_B && !pingACK_A)
    {
      ledUpdate(D2_G | D3_B| D4_G);
    }
    else if(pingACK_A && pingACK_B)
    {
      ledUpdate(D2_G | D3_G| D4_G);
    }
    else
    {
      ledUpdate(D2_B | D3_B| D4_G);
    }
    //PINGING AND RECEIVING BOT A 

    Serial.write("a");
    Serial.write(field);
    Serial.write("A");
    Serial.write("PING-----");
    
    unsigned long time = millis();
    goalBuffer[2] = 'A';
    bool ackA_flag = 0;
    while (((millis()- time) < 50))
    {
      if ((Serial.available() >= 12))
      {
        if(Serial.peek() != 'a')
        {
          Serial.read();
          continue;
        }
        if (compareBuffer())
        {              
          ackA_flag = 1;
          break;
        }
      }
    }
    pingACK_A=ackA_flag;
    delay(10);
    //PINGING AND RECEIVING BOT B
    
    Serial.write("a");
    Serial.write(field);
    Serial.write("B");
    Serial.write("PING-----");

    time = millis();
    goalBuffer[2] = 'B';
    bool ackB_flag = 0;
    while (((millis()- time) < 50))
    {
      if ((Serial.available() >= 12))
      {
        if(Serial.peek() != 'a')
        {
          Serial.read();
          continue;
        }
        if(compareBuffer())
        {
          ackB_flag = 1;
          break;         
        }
      }
    }
    pingACK_B=ackB_flag; 
  }
  delay(10);
}

void loop() 
{
  static int state = MATCH_INIT;
  static int flagA = 0;
  static int flagB = 0;

  if(state==MATCH_INIT)
  {
    ledUpdate(D1_R | D2_R | D3_R);
    state = MATCH_ON;
    startRobot('X');
  }
  if(state==MATCH_ON)
  {
    if(!digitalRead(ButtonS1))
    {
      ledUpdate(D4_G);
      stopRobot('X');
      state = MATCH_OFF;
      stateMenu = 3;
      stopACK_A=0;
      stopACK_B=0;
      delay(200);
    }
    if(!digitalRead(ButtonS2))
    { 
       stateMenu = 1;
       delay(20);
       while(!digitalRead(ButtonS2));
       delay(20);
    }
    if(!digitalRead(ButtonS3))
    {
       stateMenu = 2;
       delay(20);
       while(!digitalRead(ButtonS3));
       delay(20);
    }
    if (stateMenu == 1)
    {
      goalBuffer[2]='B';
      stopRobot('B');
      //unsigned long time = millis();
      //ledUpdate(D2_B | D3_R | D1_R);
      
      uint32_t time = millis();
      while (((millis()- time) < 200))
      {
        static int counter = 0;
        if (!(counter %10))
        {
          stopRobot('B');       
        }       
        if ((Serial.available() >= 12))
        {
          if(Serial.peek() != 'a')
          {
            Serial.read();
            continue;
          }
          if(compareBuffer())
          {
              stateMenu = 0;
              stopACK_B = 1;
              break;         
          }
        } 
        stateMenu=0;
        counter ++;
        stopACK_B = 0;
      }

      stateMenu = 0;
      delay(100);
    }
    else if (stateMenu ==2)
    {
      
      goalBuffer[2]='A';
      stopRobot('A');
      unsigned long time = millis();
      //ledUpdate(D2_R | D3_B | D1_R);
      
      while (((millis()- time) < 200))
      {
        static int counter =0;
        if (!(counter %10))
        {
          stopRobot('A'); 
        }
        
        if ((Serial.available() >= 12))
        {
          if(Serial.peek() != 'a')
          {
            Serial.read();
            continue;
          }
          if(compareBuffer())
          {
              stateMenu = 0;
              stopACK_A = 1;
              break;         
          }
        }
        stateMenu=0;
        counter ++;
        stopACK_A =0;
      }
      stateMenu = 0;
      delay(100);
    }
    if (stateMenu == 3)
    {
      ledUpdate(D4_G);
      stopRobot('X');
      state = MATCH_OFF;
    }
    if (stateMenu == 0)
    {
      state = MATCH_ON;
      if(stopACK_A && stopACK_B)
        ledUpdate(D2_G | D3_G | D1_R);
      else if(!stopACK_A && stopACK_B)
        ledUpdate(D2_G | D3_R | D1_R);
      else if(stopACK_A && !stopACK_B)
        ledUpdate(D2_R | D3_G | D1_R);
      else if(!stopACK_A && !stopACK_B)
        ledUpdate(D2_R | D3_R | D1_R);
    }
  }
  if(state==MATCH_OFF)
  {
    if(!digitalRead(ButtonS4))
    {
      ledUpdate(D1_R | D2_R | D3_R);
      startRobot('X');
      state = MATCH_ON;
      stateMenu=0;
    }
  }
}
void stopRobot(int robotID)
{
  for(int i=0; i<10; i++)
  {
    Serial.write("a");
    Serial.write(field);
    Serial.write(robotID);
    Serial.write("STOP-----");
    delay(10);
  }
}
void startRobot(int robotID)
{
  for(int i=0; i<10; i++)
  {
    Serial.write("a");
    Serial.write(field);
    Serial.write(robotID);
    Serial.write("START----");
    delay(10);
    
  }
}

bool compareBuffer()
{
  for (int i=0;i<12;i++)
  {
    if (Serial.read()!=goalBuffer[i])
    {
      return 0;
    }                          
  }
  return 1;
}
void ledUpdate(uint16_t led_state) 
{
  byte registerOne = highByte(led_state);
  byte registerTwo = lowByte(led_state);
  
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, registerOne);
  shiftOut(dataPin, clockPin, MSBFIRST, registerTwo);
  digitalWrite(latchPin, HIGH);
}
